"""
@file mcp9808.py
@brief This file contains the mcp9808 class used to initialize the external temeprature sensor.
@details This file  sets up the mcp9808 temperature sensor and communicates using I2C. This file also contains test code to show an example set up for temperature sensing. The temperature can be natively read in celsius and converted to fahrenheit if desired.
@author James Andrews
@date Feb. 08, 2021
"""
import pyb
import utime
import micropython

#Established memory reserved for displaying error messages
micropython.alloc_emergency_exception_buf(200)




class mcp9808Class:
    """ 
    @brief This Class contains the initializing, checking, celsius and fahrenheit methods used to get a temperature reading and verify the sensor is properly connected.
    """
    
    def __init__(self,address):
        """ 
        @brief This method initializes the mcp9808 temperature sensor
        @param address contains the address for the sensor
        """
        self.i2c = pyb.I2C(address,pyb.I2C.MASTER, baudrate = 100000)
    
    def check(self):
        """ 
        @brief This method checks that the sensor is properly connected and returns an error message if not.
        """
        try:
            self.i2c.mem_read(2,24,0x06)
        except:
            print("Error")
            
        self.checking = self.i2c.scan()
        return self.checking
    
    
    def celsius(self):
        """ 
        @brief This method gets the ambient temperature in degrees celsius
        """
        self.two_bytes = self.i2c.mem_read(2,24,0x05)
        self.row1 = self.two_bytes[0]
        self.row2 = self.two_bytes[1]
        self.r1 = bin(self.row1)
        self.r2 = bin(self.row2)
        self.r2_edit = self.r2[2:]
        self.r1_edit = self.r1[2:]
        
        while len(self.r2_edit) != 8:
            self.r2_edit = "0" + self.r2_edit
         
        while len(self.r1_edit) != 8:
            self.r1_edit = "0" + self.r1_edit
            
        self.combined = self.r1_edit+self.r2_edit
        self.last_13 = self.combined[-13:]
        self.temperature = 2**7*int(self.last_13[1]) + 2**6*int(self.last_13[2]) + 2**5*int(self.last_13[3]) + 2**4*int(self.last_13[4])+ 2**3*int(self.last_13[5]) + 2**2*int(self.last_13[6]) + 2**1*int(self.last_13[7]) + 2**0*int(self.last_13[8])+2**(-1)*int(self.last_13[9])+2**-2*int(self.last_13[10])+ 2**-3*int(self.last_13[11]) + 2**-4 * int(self.last_13[12])
        
        if self.last_13[0]:
            self.temperature *= 1
        else:
            self.temperature *= -1
    
        return self.temperature
    
    def fahrenheit(self,temperatureC):
        """ 
        @brief This method gets the ambient temperature in degrees fahrenheit by converting the acquired celsius value.
        @param temperatureC This parameter takes temperature in celsius and converts it into fahrenheit.
        """
        self.temperatureF = temperatureC*9/5 + 32
        return self.temperatureF





if __name__ == "__main__":
    '''
    Sample code to test the class
    '''
    try:
        tempC = []
        tempF = []
        sensor = mcp9808Class(1)
        #print(sensor.check())
        for i in range(20):
            current_celsius_value = sensor.celsius()
            print(current_celsius_value)
            current_fahrenheit_value = sensor.fahrenheit(current_celsius_value)
            print(current_fahrenheit_value)
            tempC.append(current_celsius_value)
            tempF.append(current_fahrenheit_value)
            utime.sleep(1)
                
    except KeyboardInterrupt:
        print('Program Closed')