"""
@file Lab04.py
@brief This lab contains the plot of the 8 hours worth of temperature data that was recorded on the Nucleo. The temperatures are read from the board sensor and the mcp9808 external temperature sensor.
@details In this lab, the temperature data is recorded directly on the nucleo and saved as a csv file. This file's job is to read that csv file and plot the data using Matplotlib
@author James Andrews
@date Feb. 08, 2021
"""

from matplotlib import pyplot
import csv


time_arr = []
tempB_arr = []
tempF_arr = []
tempC_arr = []
i=0
# Reads the temperature data and plots
# The built-in temperature sensor is broken.
with open('temperature_data_8hr.csv', 'r') as file:
    reader = csv.reader(file)
    for row in reader:
        time_arr.append(i)
        tempB_arr.append(float(row[1]))
        tempC_arr.append(float(row[2]))
        tempF_arr.append(float(row[3]))
        i+=1

pyplot.plot(time_arr, tempB_arr,"-r", label = "Board Temperature")
pyplot.plot(time_arr,tempC_arr,"-b", label = "MCP9808 Celsius Temperature")
pyplot.plot(time_arr, tempF_arr,"-g", label = "MCP9808 Fahrenheit Temperature")
pyplot.legend(loc = "upper right")
pyplot.title ("Temperature vs. Time")
pyplot.xlabel("Time (minutes)")
pyplot.ylabel("Temperature")
pyplot.ylim(-5,150)
pyplot.show()
print(time_arr)