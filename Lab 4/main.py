"""
@file main.py
@brief This file records 8 hours worth of temperature data and saves it as a csv file.
@details This file takes measurements using the board thermometer and an external mcp9808 temperature sensor. The values are saved and recorded as a csv file. This file uses the mcp9808 module to calculate the values.
@author James Andrews
@date Feb. 02, 2021
"""

import pyb
import utime
import micropython
import mcp9808
import array

#Established memory reserved for displaying error messages
micropython.alloc_emergency_exception_buf(200)

adc = pyb.ADCAll(12,0x70000)

def boardTemp():
    """ 
    @brief This function calculates the ambient temperature according to the temperature sensor built-in to the nucleo.
    @param This function sets up the ADC and reads the temperature using a built in function.
    """
    temperatureB = adc.read_core_temp()
    return round(temperatureB,2)

try:
    # Records all the data points as numerical arrays.
    time = array.array('H', [])
    tempB = array.array('f', [])
    tempC = array.array('H', [])
    tempF = array.array('H', [])
    sensor = mcp9808.mcp9808Class(1)
    start_time = utime.ticks_ms()
    print("The register should read 24 if the ID is correct: " + str(sensor.check()))
    for i in range(480):
        current_time = utime.ticks_ms()
        time_value = utime.ticks_diff(current_time, start_time)
        time_value1 = int(time_value)
        time.append(time_value1)
        current_celsius_value = int(sensor.celsius())
        current_fahrenheit_value = int(sensor.fahrenheit(current_celsius_value))
        tempC.append(current_celsius_value)
        x = boardTemp()
        tempB.append(x)
        tempF.append(current_fahrenheit_value)
        utime.sleep(60)
        
        
        
    # Writes the data to a csv
    with open ("temperature_data_8hr.csv","w") as csv_file:
        for idx in range(len(tempB)):
            csv_file.write("{:},{:},{:},{:}\r\n".format(time[idx],tempB[idx],tempC[idx],tempF[idx]))
    
except KeyboardInterrupt:
    print("{:},{:},{:},{:}\r\n".format(time,tempB,tempC,tempF))
    print('Program Closed')

'''
ampy --port COM3 put main.py main.py
ampy --port COM3 put mcp9808.py mcp9808.py
'''