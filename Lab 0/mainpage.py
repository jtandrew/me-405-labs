'''
@file mainpage.py
@brief the main script that organizes the files of my projects into this HTML page
@mainpage

@section sec_intro Introduction
Welcome to my online documentation of my python projects. My name is James Andrews and I am a mechanical engineering student at cal poly.



@section sec_fibonacci Fibonacci Sequence Project
I created a script that allows the user to determine the value of any index in the fibonacci sequence.

You can find the source code for my fibonacci sequence program here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab1/Fibonacci_Sequence.py



@section sec_elevator Elevator Controls Project
I created a script that simulates an elevator being powered up, moving to the first floor and then waiting for users on either floor to call the elevator.
@image html elevator_state_diagram.png width=400px

You can find the source code for my fibonacci sequence program here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/HW1/Elevator.py



@section sec_led Blinking Nucleo LED Project
I created a script that changes the brighness of an on-board Nucleo LED based on a graph (linear step function). It also turns on and off a virtual LED at the same time to show the state machine operating independently.

You can find the source code for my blinking LED program here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab2/blink_led.py

You can find the source code for two example tasks here:
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab2/main_blink.py



@section sec_encoder Encoder Project
I created a script that uses an encoder driver class, an encoder finite state machine and a user interface finite state machine to conistently update the encoder and retrieve desired values for the motor position.


@image html encoder_state_diagram.png width=400px
@image html encoder_state_diagram_user_interface.png width=400px

You can find the source code for my encoder driver class here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab3/encoder.py

You can find the source code for my encoder finite state machine here:
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab3/fsm_encoder.py

You can find the source code for my user interface finite state machine here:
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab3/ui_encoder.py

You can find the source code for emy shared variables file here:
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab3/shares.py

You can find the source code for the main file that runs both finite state machines here:
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab3/main_encoder.py



@section sec_encoderdatacollection Encoder Data Collection Project
I created a script that uses a finite state machine to collect encoder position data, plot it and save it as a csv.

Unfortunately, my Nucelo froze/broke the night before this lab was due. You said to add this in the comments.

@image html lab4_taskdiagram.png width=500px
@image html lab4_statediagram.png width=500px

You can find the source code for my user interface script here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab4/UI_front.py

You can find the source code for my data collection class here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab4/UI_data.py

You can find the source code for my main Nucleo script here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab4/main.py


@section sec_blinkingledwithapp Blinking a LED with a Bluetooth App
I created a script that uses a finite state machine to blink an led at an inputted frequency. The frequency is determined based on what the user inputs from the android based app.


@image html blinkingled_taskdiagram.png width=500px
@image html blinkingled_statediagram.png width=500px

You can find the source code for my main Nucleo script here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab5/main.py

You can find the source code for my user interface and finite state machine here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab5/UI_Task.py

You can find the source code for my Bluetooth Driver class here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab5/Bluetooth_Driver.py



@section sec_motorspeedcontrol Controller Motor Speed
I created a script that uses a finite state machine to control motor speed. The response plots shown below show tuning the Kp to improve the system response. All three show steady-state error, but as I increased the Kp value, the response became faster.


@image html lab6_figure.png width=500px
@image html lab6_taskdiagram.png width=500px
@image html lab6_statediagram.png width=500px


You can find the source code for my main Nucleo script here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab6/main6.py

You can find the source code for my Controller Task (FSM) here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab6/ControllerTask6.py

You can find the source code for my User Interface class here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab6/UserInterfaceLab6.py
    
You can find the source code for my Motor Driver class here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab6/MotorDriver6.py
    
You can find the source code for my Encoder Driver class here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab6/EncoderDriver6.py
    


@section sec_motorspeedandpositioncontrol Motor Speed and Position Control
I created a script that uses a finite state machine to control motor speed and position. The response plot shown below shows the actual system response versus the reference values. The position of my motor is very accurate relative to the reference, but the velocity oscillates between the appropriate velocity. The likely cause of this issue is the Kp value not being small enough to avoid rapidly turning the motor on and off. However, if I shrunk the Kp value, the motor would not be responsive enough to stay on the correct position.

I only included the response subplots because this lab follows the same task diagram and state diagram as the previous lab.

@image html lab7_figure.png width=500px



You can find the source code for my main Nucleo script here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab7/main7.py

You can find the source code for my Controller Task (FSM) here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab7/ControllerTask7.py

You can find the source code for my User Interface class here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab7/UserInterfaceLab7.py
    
You can find the source code for my Motor Driver class here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab7/MotorDriver7.py
    
You can find the source code for my Encoder Driver class here: 
https://bitbucket.org/jtandrew/me-305-labs/src/master/Lab7/EncoderDriver7.py
    
'''

