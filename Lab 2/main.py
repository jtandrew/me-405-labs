"""
@file Lab02.py
@brief Documentation for a program that records your reaction time.
@details This file acts 
@author James Andrews
@date Jan. 24, 2021
"""
import pyb
import micropython
import urandom

#Established memory reserved for displaying error messages
micropython.alloc_emergency_exception_buf(200)

#Initializes the LED on the Nucleo
pin5 = pyb.Pin(pyb.Pin.cpu.A5, mode = pyb.Pin.OUT_PP)
pin5.low()

#Initializes a timer and a global variable that stores the count when the button is pressed.
reactioncount = None
tim2 = pyb.Timer(2,period = 0xFFFFFFF, prescaler = 79)


def myCallback(line):
    """ 
    @brief Callback which runs when the user presses a the blue button on the Nucleo.
    @param line This parameter is needed to show when the callback has been activated.
    """
    global reactioncount
    reactioncount = tim2.counter() 

extInt = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.OUT_PP, callback = myCallback)

reactiontimes = []
while True:
    try:
        #Generates a random delay between 2 and 3 seconds
        randomvalue = urandom.randrange(2000,3000)
        pyb.delay(randomvalue)
        print('The delay was ' + str(randomvalue) + ' milliseconds.')
        
        #Turns on the LED for 1 second and stores the initial count
        pin5.high()
        global initialcount
        initialcount= tim2.counter()
        pyb.delay(1000)
        pin5.low()
        
        
        pyb.delay(5000)
        if reactioncount == None:
            #if the callback was not utilized, then the user did not press the button with 5 seconds
            print('Button was not pressed in time.')
            
        elif reactioncount <= initialcount:
            #if the user presses the button before the LED has turned on, the user pressed the button too early
            print('Button pressed too early')
            
        else:
            #records the user reaction time
            reactiontimes.append(reactioncount-initialcount)
            #print(reactiontimes)
            print('Reaction time recorded.')
            reactioncount=None
            
    except KeyboardInterrupt:
        #if the user presses ctrl+c, the loop ends and the average of the reaction times is calculated
        if len(reactiontimes) > 0:
            average = sum(reactiontimes)/len(reactiontimes)*10**-6
            print('Average Reaction Time: ' + str(average) + ' seconds')
        else:
            print('No reaction times were recorded.')
        break 
    
