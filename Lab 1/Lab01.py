"""
@file Lab01.py
@brief Documentation a vending machine where you insert coins and select a drink.
@details This file acts as a finite state machine for a vending machine. The user inputs coins by inputting numbers and selects a drink based on inputting characters.
@author James Andrews
@date Jan. 12, 2021
"""

"""Initialize code to run FSM (not the init state). This code initializes the
program, not the FSM
"""
import keyboard
import time
import HW1


def on_keypress (thing):
    """ 
    @brief Callback which runs when the user presses a key.
    @param thing This parameter is the key that is pressed.
    """
    
    global pushed_key

    pushed_key = thing.name

def printWelcome():
    """@brief Prints a VendotronˆTMˆ welcome message with beverage prices
    """
    print("""Welcome to James's Vendotron. The prices for beverages are as follows:
          'c' for Cuke: $1.05
          'p' for Popsi: $1.04
          's' for Spryte: $1.02
          'd' for Dr. Pupper: $5.50""")

state = 0
payment_list = [0,0,0,0,0,0,0,0]
cuke_price = 1.05
popsi_price = 1.04
spryte_price = 1.02
dr_pupper_price = 5.50
pushed_key = None

while True:
    """Implement FSM using a while loop and an if statement
    will run eternally until user presses CTRL-C
    """
    keyboard.on_press (on_keypress)
    """ @brief This function comes from source code here: https://bitbucket.org/spluttflob/me405-support/src/master/vend_kb.py
    """
    if state == 0:
        """perform state 0 operations 
        this init state, initializes the FSM itself, with all the
        code features already set up
        """
        printWelcome()
        state = 1 # on the next iteration, the FSM will run state 1

    elif state == 1:
        """perform state 1 operations
        """
        try:
            # If a key has been pressed, check if it's a key we care about
            if pushed_key:
                if pushed_key == '0' or pushed_key == '1' or pushed_key == '2' or pushed_key == '3' or pushed_key == '4' or pushed_key == '5' or pushed_key == '6' or pushed_key == '7':
                    state = 2
                
                elif pushed_key == 's' or pushed_key == 'd' or pushed_key == 'p' or pushed_key == 'c' or pushed_key == 'e':
                    state = 3
                else:
                    print('Command not recognized.')
                    pushed_key= None
            else:
                time.sleep(.1)
                pass
        except:
            break
        
    elif state == 2:
        """perform state 2 operations1
        """
        if pushed_key == "0":
            print ("Penny inserted")
            payment_list[0] += 1

        elif pushed_key == '1':
            print ("Nickel inserted")
            payment_list[1] += 1 

        elif pushed_key == '2':
            print('Dime inserted')
            payment_list[2] += 1 
                    
        elif pushed_key == '3':
            payment_list[3] += 1 
            print ("Quarter inserted")
                    
        elif pushed_key == '4':
            payment_list[4] += 1 
            print ("1 Dollar Bill inserted")
                    
        elif pushed_key == '5':
            payment_list[5] += 1 
            print ("5 Dollar Bill inserted")
                    
        elif pushed_key == '6':
            payment_list[6] += 1 
            print ("10 Dollar Bill inserted")
                    
        elif pushed_key == '7':
            payment_list[7] += 1 
            print ("20 Dollar Bill inserted")
        
        else:
            print('Fake Bill Detected inserted')
        pushed_key = None
        state = 1
        
        
        
    elif state == 3:
        """perform state 3 operations
        """
        current_payment_value = 0.01*payment_list[0]+0.05*payment_list[1]+0.10*payment_list[2]+0.25*payment_list[3]+1*payment_list[4]+5*payment_list[5]+10*payment_list[6]+20*payment_list[7]
        if pushed_key == "c":
            print ("Cuke: $" + str(cuke_price))
            cost = cuke_price
            if current_payment_value - cost >= 0:
                payment_tuple = tuple(payment_list)
                change = HW1.getChange(cost,payment_tuple)
                change_value = round(0.01*change[0]+0.05*change[1]+0.10*change[2]+0.25*change[3]+1*change[4]+5*change[5]+10*change[6]+20*change[7],2)
                print('Enjoy your Cuke. Your change is: $' + str(change_value) + ', which is ' + str(change) + ' in tuple form.')
                payment_list = [0,0,0,0,0,0,0,0]
                state = 1
            else:
                payment_left = cost - current_payment_value
                print('Please enter more money: $'+str(payment_left))
                state = 1

        elif pushed_key == 'p':
            print("Popsi: $" + str(popsi_price))
            cost =popsi_price
            if current_payment_value - cost >= 0:
                payment_tuple = tuple(payment_list)
                change = HW1.getChange(cost,payment_tuple)
                change_value = round(0.01*change[0]+0.05*change[1]+0.10*change[2]+0.25*change[3]+1*change[4]+5*change[5]+10*change[6]+20*change[7],2)
                print('Enjoy your Popsi. Your change is: $' + str(change_value)+ ', which is ' + str(change) + ' in tuple form.')
                payment_list = [0,0,0,0,0,0,0,0]
                state = 1
            else:
                payment_left = cost - current_payment_value
                print('Please enter more money: $'+str(payment_left))
                state = 1
        

        
        elif pushed_key == "s":
            print("Spryte: $" + str(spryte_price))
            cost = spryte_price
            if current_payment_value - cost >= 0:
                payment_tuple = tuple(payment_list)
                change = HW1.getChange(cost,payment_tuple)
                change_value = round(0.01*change[0]+0.05*change[1]+0.10*change[2]+0.25*change[3]+1*change[4]+5*change[5]+10*change[6]+20*change[7],2)
                print('Enjoy your Spryte. Your change is: $' + str(change_value) + ', which is ' + str(change) + ' in tuple form.')
                payment_list = [0,0,0,0,0,0,0,0]
                state = 1
            else:
                payment_left = cost - current_payment_value
                print('Please enter more money: $'+str(payment_left))
                state = 1 

        elif pushed_key == 'd':
            print("Dr. Pupper: $" + str(dr_pupper_price))
            cost = dr_pupper_price
            if current_payment_value - cost >= 0:
                payment_tuple = tuple(payment_list)
                change = HW1.getChange(cost,payment_tuple)
                change_value = round(0.01*change[0]+0.05*change[1]+0.10*change[2]+0.25*change[3]+1*change[4]+5*change[5]+10*change[6]+20*change[7],2)
                print('Enjoy your Dr. Pupper. Your change is: $' + str(change_value) + ', which is ' + str(change) + ' in tuple form.')
                payment_list = [0,0,0,0,0,0,0,0]
                state = 1
            else:
                payment_left = cost - current_payment_value
                print('Please enter more money: $'+str(payment_left))
                state = 1 
        
        elif pushed_key == 'e':
            cost = 0
            payment_tuple = tuple(payment_list)
            change = HW1.getChange(cost,payment_tuple)
            change_value = round(0.01*change[0]+0.05*change[1]+0.10*change[2]+0.25*change[3]+1*change[4]+5*change[5]+10*change[6]+20*change[7],2)
            print('Your change is: $' + str(change_value) + ', which is ' + str(change) + ' in tuple form.')
            state = 1 
        
        else:
            print('Not a recognized Soda brand.')
            state = 1
            
        pushed_key = None
        
    else:
        """this state shouldn't exist!
        """
        pass

print('Program is over.')
keyboard.unhook_all ()