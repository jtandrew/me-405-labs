def getChange(price, payment):
    
    payment_value = 0.01*payment[0]+0.05*payment[1]+0.10*payment[2]+0.25*payment[3]+1*payment[4]+5*payment[5]+10*payment[6]+20*payment[7]
    amount_owed = payment_value - price
    twenty_dollar_bills_count = 0
    ten_dollar_bills_count = 0
    five_dollar_bills_count = 0
    one_dollar_bills_count = 0
    quarters_count = 0
    dimes_count = 0
    nickels_count = 0
    pennies_count = 0
    
    
    while amount_owed != 0:
        if amount_owed >= 20:
            twenty_dollar_bills_count += 1
            amount_owed += -20
        elif amount_owed >= 10:
            ten_dollar_bills_count += 1
            amount_owed += -10
        elif amount_owed >= 5:
            five_dollar_bills_count += 1
            amount_owed += -5
        elif amount_owed >= 1:
            one_dollar_bills_count += 1
            amount_owed += -1
        elif amount_owed >= 0.25:
            quarters_count += 1
            amount_owed += round(-0.25,2)
        elif amount_owed >= 0.10:
            dimes_count += 1
            amount_owed += round(-.10,2)
        elif amount_owed >= .05:
            nickels_count += 1
            amount_owed += round(-0.05,2)
        elif amount_owed >= .01:
            pennies_count += 1
            amount_owed += round(-0.01,2)
        elif amount_owed < .01 and amount_owed > 0:
            amount_owed = 0
        else:
            print("You need to pay more than that!")
            break
            
    change = (pennies_count, nickels_count, dimes_count, quarters_count, one_dollar_bills_count, five_dollar_bills_count, ten_dollar_bills_count, twenty_dollar_bills_count)
    #change_value = round(0.01,2)*change[0]+round(0.05,2)*change[1]+round(0.10,2)*change[2]+round(0.25,2)*change[3]+1*change[4]+5*change[5]+10*change[6]+20*change[7]
    
    return change #change value


## Testing Code
if __name__ == "__main__":
    price = .53
    payment = (3,0,0,2,1,0,0,1)
    change = getChange(price,payment)
    #print("$" + str(change_value) + ' of change is owed to you. Thank you for shopping.')
    print("Tuple form: " +str(change))