"""
@file Lab03.py
@brief This lab utilizes serial communication to plot the voltage rise that occurs when you release the shorted blue button on the nucleo.
@details In this lab, we use serial communication to interface our front-end with our back-end on the nucleo. The user will input a command and then has 5 seconds to press the blue button on the nucleo. The front-end wait to receive the data from the back-end. Once the data ahs been received, the front-end plots the data and saves it as a csv file to a local directory.
@author James Andrews
@date Feb. 01, 2021
"""


from matplotlib import pyplot
import serial
import time
import csv


# Initialize Front-End serial communication
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)


def sendChar():
    """ 
    @brief This function send the letter G to the nucleo.
    @param This function will only accept a capital G as the input. Any other key will prompt you to press G
    """
    key = None
    while key != 'G':
        key = input("Enter a capital 'G':")
        if key == 'G':
            ser.write(str(key).encode("ascii"))
        else:
            print('You did not enter a capital G. Please try again.')
            
            
            
def cleanData(time,voltage):
    """ 
    @brief This function cleans up the voltage and time data sent by the nucleo.
    @param This function will remove all the string characters that do not belong. Then, it will convert each string value to a float for plotting later. These values are stores in lists.
    """
    voltage = voltage.replace("array('H', " , '')
    time = time.replace("array('f', " , '')
    voltage = voltage.replace(')' , '')
    voltage = voltage.replace(' ','')
    voltage = voltage.replace('[','')
    voltage = voltage.replace(']','')
    time = time.replace(')' , '')
    time = time.replace(' ','')
    time = time.replace('[','')
    time = time.replace(']','')
    time_arr = time.strip().split(',') 
    voltage_arr = voltage.strip().split(',')
    for idx in range(len(time_arr)):
        time_arr[idx] = float(time_arr[idx])
        voltage_arr[idx] = float(voltage_arr[idx])
    return time_arr,voltage_arr
    
def saveCSV(time_arr,voltage_arr):
    """ 
    @brief This function saves a csv file with the voltage and time data
    @param This function writes a csv file and saves it to the directory you are in. I have uploaded a plot of my data to the website portfolio.
    """
    with open('lab3csv.csv','w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        for idx in range(len(time_arr)):
            writer.writerow([time_arr[idx],voltage_arr[idx]])

print('Program Started')
if __name__ == '__main__':
    sendChar()
    print('You have 5 seconds to press and release the button...')
    time.sleep(5)
    try:
        myval = ser.readall().decode('ascii')
        myvalstrip= myval.strip().split(';') 
        voltage = myvalstrip[1]
        time = myvalstrip[0]
        [time_arr,voltage_arr] = cleanData(time,voltage)
        pyplot.plot(time_arr, voltage_arr,'o',markersize=2, color='black')
        pyplot.title ("ADC Voltage Count vs. Time")
        pyplot.xlabel("Time (seconds)")
        pyplot.ylabel("ADC Voltage Count")
        pyplot.show()
        saveCSV(time_arr,voltage_arr)
    except:
        print('Button release did not register. If problem persists, try pressing the black nucleo button to reset it.')

ser.close()