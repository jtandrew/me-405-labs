"""
@file main.py
@brief This file acts as the back-end for Lab 3. The voltage values are recorded here once the command has been received.
@details This file initializes the pins necessary to receive data from the nucelo board. Then, once a good dataset has been found, this file sends the data across UART to the front-end of the program. The file is sent as arrays.
@author James Andrews
@date Jan. 26, 2021
"""


import pyb
from pyb import UART
import micropython
import array

#Established memory reserved for displaying error messages
micropython.alloc_emergency_exception_buf(200)


# Initializes the button and pin needed to measure the voltage signal
pin13 = pyb.Pin(pyb.Pin.board.PC13, mode = pyb.Pin.IN)
pin0 = pyb.Pin(pyb.Pin.board.PA0, mode = pyb.Pin.IN)
myuart = UART(2)



while True:
    try:
        freq = 40000
        if myuart.any() != 0: # if the UART receives a command from the front-end.
            val = myuart.readchar() # reads the command to clear the line
            good_data = 0
            buf = array.array('H',( 0 for index in range (200))) #creates the inital array
            tim = array.array('f',((index*(1/freq)) for index in range(200)))
            while good_data != 1:
                adc = pyb.ADC(pin0)
                adc.read_timed(buf,freq) # writes the voltage values to the buf array
                
                if buf[0] <20 and buf[199] > 3700:
                    good_data = 1
                    myuart.write('{:};{:}\r\n'.format(tim,buf)) # sends the arrays over the UART

                else:
                    pass
                pass

        else:
            pass
        
    except KeyboardInterrupt:
        break 



    