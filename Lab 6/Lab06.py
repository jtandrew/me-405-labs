"""
@file Lab06.py
@brief This file contains the script that plots the results of several simulations of ball movement from different intital conditions.
@details This file uses the EOM developed in Lab 05 to solve for the motion of the ball and platform at different conditions. This solution is found using jacobian linearization. The results of each simulation are plotted.
@author James Andrews
@date Feb. 02, 2021
"""

import sympy as sym
from scipy.integrate import solve_ivp
from matplotlib import pyplot


def jacobian_fn_x(matrix):
    """ 
    @brief This function finds the jacobian with respect to x
    @param matrix This parameter is the ddtx matrix that was created using q_dot and q_double_dot
    """
    four_by_one = matrix
    Z = sym.Matrix([x,theta,x_dot,theta_dot])
    J = four_by_one.jacobian(Z)
    J_x = J.subs([(x,0),(x_dot,0),(theta,0),(theta_dot,0)])
    return J_x

def jacobian_fn_u(matrix):
    """ 
    @brief This function finds the jacobian with respect to u
    @param matrix This parameter is the ddtx matrix that was created using q_dot and q_double_dot
    """
    four_by_one = matrix
    Z = sym.Matrix([T])
    J = four_by_one.jacobian(Z)
    J_u = J.subs([(x,0),(x_dot,0),(theta,0),(theta_dot,0)])
    return J_u

def my_ode_fn_closed (time, states):
    """ 
    @brief This function solves the ODE for the closed system
    @param time This parameter sets the intial and final simulation time
    @param states This parameter sets the intial conditions of the state variables
    """
    x_position = A[0,0]*states[0] + A[0,1]*states[1] + A[0,2]*states[2] + A[0,3]*states[3] + B[0]*u[0]
    theta_position = A[1,0]*states[0] + A[1,1]*states[1] + A[1,2]*states[2] + A[1,3]*states[3] + B[1]*u[1]
    x_velocity = A[2,0]*states[0] + A[2,1]*states[1] + A[2,2]*states[2] + A[2,3]*states[3] + B[2]*u[2]
    theta_velocity = A[3,0]*states[0] + A[3,1]*states[1] + A[3,2]*states[2] + A[3,3]*states[3] + B[3]*u[3]
    
    return [x_position,theta_position,x_velocity,theta_velocity]

def my_ode_fn_open (time, states):
    """ 
    @brief This function solves the ODE for the open system
    @param time This parameter sets the intial and final simulation time
    @param states This parameter sets the intial conditions of the state variables
    """
    x_position = A[0,0]*states[0] + A[0,1]*states[1] + A[0,2]*states[2] + A[0,3]*states[3] 
    theta_position = A[1,0]*states[0] + A[1,1]*states[1] + A[1,2]*states[2] + A[1,3]*states[3] 
    x_velocity = A[2,0]*states[0] + A[2,1]*states[1] + A[2,2]*states[2] + A[2,3]*states[3] 
    theta_velocity = A[3,0]*states[0] + A[3,1]*states[1] + A[3,2]*states[2] + A[3,3]*states[3] 
    
    return [x_position,theta_position,x_velocity,theta_velocity]







# Define Constants
rm = 60/1000 # mm
lr = 50/1000 # mm
rb = 10.5/1000 # mm
rg = 42/1000 # mm
lp = 110/1000 # mm
rp = 32.5/1000 # mm
rc = 50/1000 # mm
mb = 30/1000 # g
mp = 400/1000 # g
Ip = 1.88*10**6/1000**3 # g-mm^2
B = 10 # mNm-s/rad    <--------------????????
x = sym.Symbol('x')
Ib = mb*rb**2/1000**3  #g-mm^2
g = 9.81 #m/s^2


#Define Symbolic Variables
theta = sym.Symbol('theta')
theta_dot = sym.Symbol('theta_dot')
theta_double_dot = sym.Symbol('theta_double_dot')
T = sym.Symbol('T')
x_dot = sym.Symbol('x_dot')
'''
mb = sym.Symbol('mb')
mp = sym.Symbol('mp')
rb = sym.Symbol('rb')
rc = sym.Symbol('rc')
rg = sym.Symbol('rg')
rm = sym.Symbol('rm')
Ib = sym.Symbol('Ib')
Ip = sym.Symbol('Ip')
g = sym.Symbol('g')
lp = sym.Symbol('lp')
B = sym.Symbol('B')
'''



# Equations of Motion
M = sym.Matrix([[-1*(mb*rb**2+mb*rc*rb+Ib)/rb,-1*(Ib*rb+Ip*rb+mb*rb**3+mb*rb*rc**2+2*mb*rb**2*rc+mp*rb*rg**2+mb*rb*x**2)/rb],[-1*(mb*rb**2+Ib)/rb,-1*(mb*rb**3+mb*rc*rb**2+Ib*rb)/rb]])
f = sym.Matrix([B*theta_dot - g*mb*(theta*(rb+rc)+x)+T*lp/rm+2*mb*theta_dot*x*x_dot-g*mp*rg*theta,-mb*rb*x*theta_dot**2-g*mb*rb*theta])

# Defining q_dot and q_double_dot
q_double_dot = M**-1*f
q_dot = sym.Matrix([x_dot,theta_dot])

# Defining ddt of state variables x
ddtx = sym.Matrix([q_dot,q_double_dot])


#Find the Jacobian
A = jacobian_fn_x(ddtx)
B = jacobian_fn_u(ddtx)
states = sym.Matrix([x,theta,x_dot,theta_dot])
k_values = sym.Matrix([-.3,-.2,-.05,-.02])
K_inv = sym.transpose(k_values)
u = K_inv


## Part A
results1_closed = solve_ivp (my_ode_fn_closed,[0,1],[0,0,0,0],rtol=1e-3)
results1_open = solve_ivp (my_ode_fn_open,[0,1],[0,0,0,0],rtol=1e-3)

pyplot.figure(0)
pyplot.plot(results1_closed.t,results1_closed.y[0],results1_closed.t,results1_closed.y[1],results1_closed.t,results1_closed.y[2],results1_closed.t,results1_closed.y[3])
pyplot.xlabel("Time (s)")
pyplot.title('A) Closed Loop')
pyplot.legend(('X', 'θ', 'X_dot','θ_dot'))
pyplot.figure(1)
pyplot.plot(results1_open.t,results1_open.y[0],results1_open.t,results1_open.y[1],results1_open.t,results1_open.y[2],results1_open.t,results1_open.y[3])
pyplot.xlabel("Time (s)")
pyplot.title('A) Open Loop')
pyplot.legend(('X', 'θ', 'X_dot','θ_dot'))

## Part B
results2_closed = solve_ivp (my_ode_fn_closed,[0,0.4],[0.05,0,0,0],rtol=1e-3)
results2_open = solve_ivp (my_ode_fn_open,[0,0.4],[0.05,0,0,0],rtol=1e-3)
pyplot.figure(2)
pyplot.plot(results2_closed.t,results2_closed.y[0],results2_closed.t,results2_closed.y[1],results2_closed.t,results2_closed.y[2],results2_closed.t,results2_closed.y[3])
pyplot.xlabel("Time (s)")
pyplot.title('B) Closed Loop')
pyplot.legend(('X', 'θ', 'X_dot','θ_dot'))
pyplot.figure(3)
pyplot.plot(results2_open.t,results2_open.y[0],results2_open.t,results2_open.y[1],results2_open.t,results2_open.y[2],results2_open.t,results2_open.y[3])
pyplot.xlabel("Time (s)")
pyplot.title('B) Open Loop')
pyplot.legend(('X', 'θ', 'X_dot','θ_dot'))

## Part C
results3_closed = solve_ivp (my_ode_fn_closed,[0,0.4],[0,5*2*3.14159265/180,0,0],rtol=1e-3)
results3_open = solve_ivp (my_ode_fn_closed,[0,0.4],[0,5*2*3.14159265/180,0,0],rtol=1e-3)
pyplot.figure(4)
pyplot.plot(results3_closed.t,results3_closed.y[0],results3_closed.t,results3_closed.y[1],results3_closed.t,results3_closed.y[2],results3_closed.t,results3_closed.y[3])
pyplot.xlabel("Time (s)")
pyplot.title('C) Closed Loop')
pyplot.legend(('X', 'θ', 'X_dot','θ_dot'))
pyplot.figure(5)
pyplot.plot(results3_open.t,results3_open.y[0],results3_open.t,results3_open.y[1],results3_open.t,results3_open.y[2],results3_open.t,results3_open.y[3])
pyplot.xlabel("Time (s)")
pyplot.title('C) Open Loop')
pyplot.legend(('X', 'θ', 'X_dot','θ_dot'))

## Part D
results4_closed = solve_ivp (my_ode_fn_closed,[0,.4],[0,0,1,0],rtol=1e-3)
results4_open = solve_ivp (my_ode_fn_closed,[0,.4],[0,0,1,0],rtol=1e-3)
pyplot.figure(6)
pyplot.plot(results4_closed.t,results4_closed.y[0],results4_closed.t,results4_closed.y[1],results4_closed.t,results4_closed.y[2],results4_closed.t,results4_closed.y[3])
pyplot.xlabel("Time (s)")
pyplot.title('D) Closed Loop')
pyplot.legend(('X', 'θ', 'X_dot','θ_dot'))
pyplot.figure(7)
pyplot.plot(results4_open.t,results4_open.y[0],results4_open.t,results4_open.y[1],results4_open.t,results4_open.y[2],results4_open.t,results4_open.y[3])
pyplot.xlabel("Time (s)")
pyplot.title('D) Open Loop')
pyplot.legend(('X', 'θ', 'X_dot','θ_dot'))
