'''
@file main.py
@brief A file containing a script that initializes the motors, encoders, touchpad and Controller.
@details This file initializes the hardware and repeatedly runs the controller.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''
#import all the necessary modules
import pyb
import utime
import micropython
from MotorDriver9 import Motor
from EncoderDriver9 import Encoder
from SafetyTask9 import Safety
from TouchSensor9 import TouchSensorDriver
from ControllerTask9 import Controller



# Established memory reserved for displaying error messages
micropython.alloc_emergency_exception_buf(200)

# Initialize Encoders
encoder1 = Encoder(pyb.Pin.cpu.B6,pyb.Pin.cpu.B7,4,1000)
encoder2 = Encoder(pyb.Pin.cpu.C6,pyb.Pin.cpu.C7,8,1000)

# Create sleep pin and timer for motor
pin_nSLEEP = pyb.Pin.cpu.A15
tim3 = pyb.Timer(3,freq=20000)
    
# Initialize Motors
motor1_first_pin = pyb.Pin.cpu.B4
motor1_second_pin = pyb.Pin.cpu.B5
motor1 = Motor(pin_nSLEEP, motor1_first_pin, motor1_second_pin, tim3)
motor2_first_pin = pyb.Pin.cpu.B0
motor2_second_pin = pyb.Pin.cpu.B1
motor2 = Motor(pin_nSLEEP, motor2_first_pin, motor2_second_pin, tim3)
    
# Initialize Safety Tasks
safety1 = Safety(encoder1,motor1)
safety2 = Safety(encoder2,motor2)
kill_state = 2
restore_state = 3

# Initialize TouchPad
center_x = 2030
center_y = 2130
xm_pin = pyb.Pin.cpu.A0
xp_pin = pyb.Pin.cpu.A1
yp_pin = pyb.Pin.cpu.A6
ym_pin = pyb.Pin.cpu.A7
length = 7.5
width = 4.5
touchpad = TouchSensorDriver(length,width,center_x,center_y,xm_pin,xp_pin,yp_pin,ym_pin)

# Creating the callback functions that kill power when triggered.
def myCallbackKillPower(line):
    """ 
    @brief Callback which runs when the user presses a the blue button on the Nucleo.
    @param line This parameter is needed to show when the callback has been activated.
    """
    motor1.kill = 1
    motor2.kill = 1
    print('test')
    motor1.disable()
    motor2.disable()
    
def myCallbackRestorePower(line):
    """ 
    @brief This function restores power once the user wants to continue.
    """ 
    motor1.kill = 0
    motor2.kill = 0
    extInt.disable()
    motor1.enable()
    motor2.enable()
    print('test1')
    #utime.sleep_ms(5)
    extInt.enable()


#Setting up the external interrupts
extInt = pyb.ExtInt(pyb.Pin.board.PB2, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, callback = myCallbackKillPower) # kills power if fault detected
extInt1 = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.IN, callback = myCallbackRestorePower) 

#disable the interrupt briefly so it does not interfere with the nsleep pin going high
extInt.disable()
motor1.enable()
motor2.enable()
utime.sleep_ms(5)
extInt.enable()

# Asking the user to hit 'enter' to zero the platform to its current position
input('Press enter to set the zero and start the motors.')
encoder1.set_position(0)
encoder2.set_position(0)
encoder1.update()
encoder2.update()

#initialize the controller
controller = Controller(encoder1, encoder2, motor1, motor2, touchpad)

while True:
    try:
        #repeatedly run the controller to balance the ball or platform
        controller.run()

    except KeyboardInterrupt:
        print("Program Closed")
        motor1.disable()
        motor2.disable()
        extInt.disable()
        extInt1.disable()
        break
    