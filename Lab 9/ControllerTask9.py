'''
@file ControllerTask9.py
@brief A file containing a script that controls the platform when called by the main script.
@details This file takes in the motors, encoders and touchpad information. It processes the sensors and outputs the latest value to feed into the motor. The motor rotates and balances the platform.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''
import pyb
import utime

class Controller:
    '''
    @brief      A class that controls the platform by calculating the input parameters for the gain calculation
    @details    This class takes the motors, encoders and touchpad data to calaculate the appropriate gain to send the final duty value to the motors. This runs repeatedly, constantly adjusting the platform to balance the ball.
    
    '''
    
    def __init__ (self, encoder1, encoder2, motor1, motor2, touchpad):
        
        '''
        @brief    This constructor initializes the controller object and set up the initial position and time. 
        @param encoder1 This parameter is the first encoder object.
        @param encoder2 This parameter is the second encoder object.
        @param motor1 This parameter is the first motor object.
        @param motor2 This parameter is the second motor object.
        @param touchpad This parameter is the sensor that detect the ball.
        '''
        self.motor1 = motor1
        self.encoder1 = encoder1
        self.motor2 = motor2
        self.encoder2 = encoder2
        self.touchpad = touchpad
        self.previous_time = utime.ticks_us()
        self.previous_positions = [0,0]
        self.count = 1
        self.n = 500

            
        
        
    def run(self):
        '''
        @brief    This method enables the motor by turning on the sleep pin. 
        '''
        # Update encoder and get current values
        self.encoder1.update()
        self.encoder2.update()
        self.theta_dot1 = self.encoder1.get_delta()
        self.theta_dot2 = self.encoder2.get_delta()
        self.theta1 = self.encoder1.get_position()
        self.theta2 = self.encoder2.get_position()
        
      
        # Calculating linear velocity and position on the TouchPad
        self.new_positions = self.touchpad.getPositions()
        self.new_time = utime.ticks_us()
        self.delta_x = self.new_positions[0]-self.previous_positions[0]
        self.delta_y = self.new_positions[1]-self.previous_positions[1]
        self.delta_t = self.new_time-self.previous_time
        
        # Calculating x,y,x_dot,y_dot
        self.x = self.new_positions[0]
        self.y = self.new_positions[1]
        self.x_dot = self.delta_x/self.delta_t
        self.y_dot = self.delta_y/self.delta_t
        
        # Overwrite the old values for position and time
        self.previous_positions = self.new_positions
        self.previous_time = self.new_time
        
        # Multiply the positions and velocities by the appropriate gains to get the duty cycle for each
        if self.new_positions[2] == 0:
            self.duty1 = self.motor1.calculateDuty2(self.theta_dot1,self.theta1)
            self.duty2 = self.motor2.calculateDuty2(self.theta_dot2,self.theta2)
            #print('No Ball')
        elif self.new_positions[2] == 1:
            self.duty1 = self.motor1.calculateDuty4(self.x_dot,self.theta_dot1,self.x,self.theta1)
            self.duty2 = self.motor2.calculateDuty4(self.y_dot,self.theta_dot2,self.y,self.theta2)
            #print('Ball')
        else:
            pass
        self.count +=1
        if self.count % self.n ==0:
            print("theta1: " + str(self.theta1))
            print("theta2: " + str(self.theta2))
            print("Duty1: " + str(int(self.duty1)))
            print("Duty2: " + str(int(self.duty2)))
        else:
            pass
        
            
        self.motor1.set_duty(int(self.duty1))
        self.motor2.set_duty(int(self.duty2))



