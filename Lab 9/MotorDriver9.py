'''
@file MotorDriver9.py
@brief A file containing a motor driver class. This class can be used for multiple types of motors, if you provide the correct parameters.
@details This file initializes the motor driver such that the controller task can set duty values to the two motors.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''
import pyb

class Motor:
    '''
    @brief      A class that sets up the motor with the appropriate channels and is ready for duty values.
    @details    This class contains specifications for two motors that are attached in a very specific manner to hardware consisting of a nucleo board that is on top of pre-built hardware board.
    
    '''
    
    def __init__ (self, nSLEEP_pin, first_pin, second_pin, tim):
        
        '''
        @brief    This constructor initializes the data collection class and establishes the reference omega.  
        @param nSLEEP_pin This parameter sets up the sleep pin to enable and disable the motor.
        @param first_pin This parameter is the first Pyb.Pin that the motor is connected to.
        @param second_pin This parameter is the second Pyb.Pin that the motor is connected to.
        @param tim This parameter is the initialized timer for the motor.
        '''
        self.sleep_pin = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        self.sleep_pin.low()
        self.first_pin = pyb.Pin(first_pin, pyb.Pin.OUT_PP)
        self.second_pin =  pyb.Pin(second_pin, pyb.Pin.OUT_PP)
        self.motortype = 0
        self.kill = 0
        self.max_duty = 70

        #initializes different channels for each motor, depending on the values for the pins
        if str(self.first_pin) == 'Pin(Pin.cpu.B4, mode=Pin.OUT)':
            if str(self.second_pin) == 'Pin(Pin.cpu.B5, mode=Pin.OUT)':
                self.motortype = 1
                self.pin4 = self.first_pin 
                self.pin5 = self.second_pin
                self.t3ch1 = tim.channel(1,pyb.Timer.PWM,pin=self.pin4)
                self.t3ch2 = tim.channel(2,pyb.Timer.PWM,pin=self.pin5)
            else:
                print('1. The second pin does not match a reasonable first pin.')
        
        elif str(self.first_pin) == 'Pin(Pin.cpu.B0, mode=Pin.OUT)':
            if str(self.second_pin) == 'Pin(Pin.cpu.B1, mode=Pin.OUT)':
                self.motortype = 2
                self.pin0 = self.first_pin 
                self.pin1 = self.second_pin
                self.t3ch3 = tim.channel(3,pyb.Timer.PWM,pin=self.pin0)
                self.t3ch4 = tim.channel(4,pyb.Timer.PWM,pin=self.pin1)
            else:
                print('2. The second pin does not match a reasonable first pin.')
        
        else:
            print('The first pin was not correctly inputted.')
            
        
        
    def enable(self):
        '''
        @brief    This method enables the motor by turning on the sleep pin. 
        '''
        if self.kill == 0:
            self.sleep_pin.high()
            print("motor enabled")
        elif self.kill == 1:
            print("Fix the fault first")
        else:
            print("Nope")
        
    def disable(self):
        '''
        @brief    This method disables the motor by turning off the sleep pin.  
        '''
        self.sleep_pin.low()
        print("Power was killed.")


    def set_duty(self, duty):
        '''
        @brief    This method takes the duty cycle and rotates the motor in the appropriate direction based on the sign of the duty. 
        @param duty This parameter is the value for the duty you want to set the motor to.
        '''
        
        #depending on which motor you want rotated(motortype) the timer channels that receive the PWM varies.
        if self.motortype == 1:
            if duty >= 0 and duty <=self.max_duty: # positive duty means forward movement
                #print(duty)
                self.t3ch2.pulse_width_percent(0)
                self.t3ch1.pulse_width_percent(abs(duty))
                
            elif duty > self.max_duty: # positive duty means forward movement
                #print(duty)
                self.t3ch2.pulse_width_percent(0)
                self.t3ch1.pulse_width_percent(self.max_duty)
                
            elif duty < 0 and duty >= -1*self.max_duty: # negative duty means reverse movement
                #print(duty)
                self.t3ch1.pulse_width_percent(0)
                self.t3ch2.pulse_width_percent(abs(duty))
                
            elif duty < -1*self.max_duty: # negative duty means reverse movement
                #print(duty)
                self.t3ch1.pulse_width_percent(0)
                self.t3ch2.pulse_width_percent(self.max_duty)
            
            else:
                print('Not this one')
                self.disable()
            
        elif self.motortype == 2:
            if duty >= 0 and duty <=self.max_duty: # positive duty means forward movement
                #print(duty)
                self.t3ch4.pulse_width_percent(0)
                self.t3ch3.pulse_width_percent(abs(duty))
            
            elif duty > self.max_duty:
                #print(duty)
                self.t3ch4.pulse_width_percent(0)
                self.t3ch3.pulse_width_percent(self.max_duty)
                
            elif duty < 0 and duty >= -1*self.max_duty:
                #print(duty)
                self.t3ch3.pulse_width_percent(0)
                self.t3ch4.pulse_width_percent(abs(duty))
                
            elif duty < -1*self.max_duty:
                #print(duty)
                self.t3ch3.pulse_width_percent(0)
                self.t3ch4.pulse_width_percent(self.max_duty)
                
            else:
                print('Not this one too')
                self.disable()
        else:
            print('You cannot set the duty of a motor that does not exist.')
            pass

    def calculateDuty4(self, x_dot, theta_dot, x, theta):
        '''
        @brief    This method calculates the duty based on the feedback from the encoder and touchsensor when the ball is on the platform.
        @param x_dot This parameter is linear velocity of the ball in one direction.
        @param theta_dot This parameter is the angular velocity of the motor arm.
        @param x This parameter is the linear distance of the ball relative to the center of the plate.
        @param theta This parameter is the angle of the motor arm relative to the starting point.
        '''
        #if the ball is on the touch sensor, this gain value will be calculated
        self.K1 = -6.5
        self.K2 = -4
        self.K3 = -40
        self.K4 = -3
        self.K = self.K1*x_dot+self.K2*theta_dot+self.K3*x+self.K4*theta
        
        return self.K
    
    def calculateDuty2(self, theta_dot, theta):
        '''
        @brief    This method calculates the duty based on the feedback from the encoder when the ball is not on the platform.
        @param theta_dot This parameter is the angular velocity of the motor arm.
        @param theta This parameter is the angle of the motor arm relative to the starting point.
        '''
        #if the ball is not on the touch sensor, this gain value will be calculated
        self.K2 = -.5
        self.K4 = -10 
        self.K = self.K2*theta_dot+self.K4*theta
        return self.K