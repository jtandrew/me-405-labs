'''
@file EncoderDriver9.py
@brief A file containing an encoder driver class. This class can be used for multiple encoders if you provide the correct inputs.
@details This file initializes encoders such that the position of the motor can be found. It also contains a method that can provide the degrees that the motor has rotated during each period of time.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''
import pyb

class Encoder:
    '''
    @brief      A class that sets up the encoder and allows for an encoder change in angle to be given at any point in time.
    @details    This class implements encoder information to properly understand what the motor position and change in angle are.
    
    '''
    
    def __init__(self,Pin_1_Location, Pin_2_Location,timer,cpr): # must enter location as 'py.Pin.CPU. " with Pin at the end
        '''
        @brief      Sets up the encoder driver to be configured properly using 4 parameters that define the type of motor being used.
        @param Pin_1_Location This parameter is the first pyb.Pin location for the encoder 
        @param Pin_2_Location This parameter is the second pyb.Pin location for the encoder 
        @param timer This parameter that initailizes the timer for the encoder.
        @param cpr This parameter is the clocks per revolution of the encoder.
        '''
        
        self.Period = 0xffff #defines the period
        self.tim = pyb.Timer(timer) # initializes the timer based on the input parameter
        self.tim.init(prescaler=0,period=self.Period)
        self.tim.channel(1,pin=Pin_1_Location,mode = pyb.Timer.ENC_AB) # sets up the first encoder channel
        self.tim.channel(2,pin=Pin_2_Location,mode = pyb.Timer.ENC_AB) # sets up the second encoder channel
        self.count = self.tim.counter() # gets the first count
        self.motor_position = 0 # starts the motor position at zero
        self.gear_ratio = 4
        self.quad_encoder = 4
        self.deg_per_tick = 1/(cpr*self.quad_encoder/360) # degrees per tick



        
    def update(self):
        '''
        @brief      Updates the recorded position of the encoder by calculating the appropriate delta
        '''
        
        self.previous_count = self.count #sets the current count value to be the previous value to free up this attribute for the next count
        self.count = self.tim.counter() # gets the current count value
        
        if abs(self.count - self.previous_count) > self.Period/2: # This algorithm sorts the deltas into good and bad deltas. If the delta is larger than half the period, we know it is bad.
            self.delta = self.count - self.previous_count #calculates the bad delta
            if self.delta > 0: # if the bad delta is positive, we know that the good delta would be the bad delta minus the period
                self.good_delta = (self.delta - self.Period)*self.deg_per_tick
            elif self.delta < 0: # if the bad delta is negative, we know that the good delta would be the bad delta plus the period
                self.good_delta = (self.delta + self.Period)*self.deg_per_tick
                
        else: # if the delta is less than half the period, the good delta is the delta and no adjustments must be made
            self.delta = self.count -self.previous_count #calculates the delta
            self.good_delta = self.delta*self.deg_per_tick #sets the good delta to this delta
        
            
        self.motor_position += self.good_delta # adds the good delta value to the total motor position
        
        
        
    def get_position(self):
        '''
        @brief      This method returns the current position of the motor
        '''
        return self.motor_position
    
        
        
    def set_position(self, position):
        '''
        @brief      This method sets the current position of the motor to the input parameter value
        @param position This parameter is the value you want to set the motor position to initially. This is the way to zero the device.
        '''
        self.previous_count = self.tim.counter()
        self.motor_position = position
        

    def get_delta(self):
        '''
        @brief      This method returns the correct delta
        '''
        #print('Encoder: ' + str(self.good_delta))
        return self.good_delta