"""
@file Lab07.py
@brief This file establishes the touch sensor driver that will be used in the final project to balance the ball.
@details This file takes the length and width of the touchpad sensor and uses resistors to determine the position of the ball on the platform. These coordinates will be used by the FSm in the final project to control the ball position.
@author James Andrews
@date Feb. 23, 2021
"""


import pyb
import utime
import micropython

class TouchSensor:
    '''
    @brief      A class that initializes the touchpad sensor.
    @details    This class takes the pressure on the sensor and gives out the coordinates of the ball.
    
    '''
    
    def __init__(self, length , width , center_x, center_y , xm , xp, yp , ym): # must enter location as 'py.Pin.CPU. " with Pin at the end
        '''
        @brief      Sets up the initial atrributes of the sensor.
        @param length This parameter is the x distance of the touchpad sensor 
        @param width This parameter is the y distance of the touchpad sensor
        @param center_x This parameter is the center of the length
        @param center_y This parameter is the center of the length
        @param xm This parameter is the pin address for xm
        @param xp This parameter is the pin address for xp
        @param ym This parameter is the pin address for ym
        @param yp This parameter is the pin address for yp
        '''
        self.center = [center_x,center_y]
        self.length = length
        self.width = width
        self.z_threshold = 3700
        self.xm = pyb.Pin(xm) # Pin 1
        self.xp = pyb.Pin(xp) # Pin 2
        self.yp = pyb.Pin(yp) # Pin 3
        self.ym = pyb.Pin(ym) # Pin 4
        self.x_position_filtered = 0
        self.y_position_filtered = 0

    @micropython.native 
    def scanX(self):
        '''
        @brief      Determines the x-coordinate of the ball on the touchpad sensor. These values are filtered. 
        '''
        self.xm.init(pyb.Pin.OUT_PP)  # Pin 1
        self.xp.init(pyb.Pin.OUT_PP) # Pin 2
        self.yp.init(pyb.Pin.IN) # Pin 3
        self.ym.init(pyb.Pin.IN)  # Pin 4
        
        self.xp.high()
        self.xm.low()
        self.ym_adc = pyb.ADC(self.ym)
        utime.sleep_us(4)
        self.x_position = self.ym_adc.read() - self.center[0]
        self.x_position_filtered = .2*self.x_position_filtered+.8*self.x_position
        
        return self.x_position_filtered
    
    @micropython.native
    def scanY(self):
        '''
        @brief      Determines the y-coordinate of the ball on the touchpad sensor. These values are filtered.
        '''
        self.xm.init(pyb.Pin.IN) # Pin 1
        self.xp.init(pyb.Pin.IN) # Pin 2
        self.yp.init(pyb.Pin.OUT_PP) # Pin 3
        self.ym.init(pyb.Pin.OUT_PP)  # Pin 4
        
        self.yp.high()
        self.ym.low()
        self.xm_adc = pyb.ADC(self.xm)
        utime.sleep_us(4)
        self.y_position = self.xm_adc.read() - self.center[1]
        self.y_position_filtered = .2*self.y_position_filtered+.8*self.y_position
        
        return self.y_position_filtered

    @micropython.native    
    def scanZ(self):
        '''
        @brief      Determines if the touchpad sensor has anything touching it. 
        
        '''
        self.xm.init(pyb.Pin.OUT_PP) # Pin 1
        self.xp.init(pyb.Pin.IN) # Pin 2
        self.yp.init(pyb.Pin.OUT_PP) # Pin 3
        self.ym.init(pyb.Pin.IN)  # Pin 4
        
        self.yp.high()
        self.xm.low()
        self.ym_adc = pyb.ADC(self.ym)
        utime.sleep_us(4)
        self.z_value = self.ym_adc.read()
        if self.z_value < self.z_threshold:
            self.z_position = 1
        else:
            self.z_position = 0
        return self.z_position

    @micropython.native
    def getPositions(self):
        '''
        @brief      Calls three methods to get the x, y and z coordinates
        '''
        self.x_length_count = 4020 - 200
        self.y_length_count = 3650 - 500
        self.x_conversion = self.length/self.x_length_count # inches per count
        self.y_conversion = self.width/self.y_length_count
        self.x_position = round(self.scanX()*self.x_conversion,2)
        self.y_position = round(self.scanY()*self.y_conversion,2)
        self.z_position = self.scanZ()
        self.tuple = (self.x_position,self.y_position,self.z_position)
        return self.tuple
        
if __name__ == "__main__":
    
    
    center_x = 2030
    center_y = 2130
    xm_pin = pyb.Pin.cpu.A0
    xp_pin = pyb.Pin.cpu.A1
    yp_pin = pyb.Pin.cpu.A6
    ym_pin = pyb.Pin.cpu.A7
    length = 7.5
    width = 4.5
    touchpad = TouchSensor(length,width,center_x,center_y,xm_pin,xp_pin,yp_pin,ym_pin)
    try:
        for i in range(1000):
            t0 = utime.ticks_us()
            values = touchpad.getPositions()
            t1 = utime.ticks_us()
            time_to_values = utime.ticks_diff(t1,t0)
            print("Values (x,y,z): " + str(values))
            print("Total Time Elapsed: " + str(time_to_values) + " microseconds")
            
    except KeyboardInterrupt:
        pass