'''
@file MotorDriver8.py
@brief A file containing a motor driver.
@details This file initializes the motor driver such that the controller task can set duty values to the two motors.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''
import pyb

class Motor:
    '''
    @brief      A class that sets up the motor with the appropriate channels and is ready for duty values.
    @details    This class contains specifications for two motors that are attached in a very specific manner to hardware consisting of a nucleo board that is on top of pre-built hardware board.
    
    '''
    
    def __init__ (self, nSLEEP_pin, first_pin, second_pin, tim):
        
        '''
        @brief    This constructor initializes the data collection class and establishes the reference omega.  
        '''
        self.sleep_pin = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        self.sleep_pin.low()
        self.first_pin = pyb.Pin(first_pin, pyb.Pin.OUT_PP)
        self.second_pin =  pyb.Pin(second_pin, pyb.Pin.OUT_PP)
        self.motortype = 0
        self.kill = 0
        
        if str(self.first_pin) == 'Pin(Pin.cpu.B4, mode=Pin.OUT)':
            if str(self.second_pin) == 'Pin(Pin.cpu.B5, mode=Pin.OUT)':
                self.motortype = 1
                self.pin4 = self.first_pin 
                self.pin5 = self.second_pin
                self.t3ch1 = tim.channel(1,pyb.Timer.PWM,pin=self.pin4)
                self.t3ch2 = tim.channel(2,pyb.Timer.PWM,pin=self.pin5)
            else:
                print('1. The second pin does not match a reasonable first pin.')
        
        elif str(self.first_pin) == 'Pin(Pin.cpu.B0, mode=Pin.OUT)':
            if str(self.second_pin) == 'Pin(Pin.cpu.B1, mode=Pin.OUT)':
                self.motortype = 2
                self.pin0 = self.first_pin 
                self.pin1 = self.second_pin
                self.t3ch3 = tim.channel(3,pyb.Timer.PWM,pin=self.pin0)
                self.t3ch4 = tim.channel(4,pyb.Timer.PWM,pin=self.pin1)
            else:
                print('2. The second pin does not match a reasonable first pin.')
        
        else:
            print('The first pin was not correctly inputted.')
            
            
    def kill_power(self):
        '''
        @brief    This method disables the motor and changes the state to be in the fault state such that the motors cannot be enabled until the fault is cleared. 
        '''
        self.disable()
        self.kill = 1
        print("Power was killed")
        
    def restore_power(self):
        '''
        @brief    This method allows the motor to be enabled again.
        '''
        self.kill = 0
        print("Power was restored")
        
        
    def enable(self):
        '''
        @brief    This method enables the motor by turning on the sleep pin. 
        '''
        if self.kill == 0:
            self.sleep_pin.high()
        elif self.kill == 1:
            print("Fix the fault first")
        else:
            print("Nope")
        
    def disable(self):
        '''
        @brief    This method disables the motor by turning off the sleep pin.  
        '''
        self.sleep_pin.low()


    def set_duty(self, duty):
        '''
        @brief    This method takes the duty cycle and rotates the motor in the appropriate direction based on the sign of the duty.  
        '''
        self.motor.enable()
        if self.motortype == 1:
            if duty > 0 and duty <=100: # positive duty means forward movement
                #print(duty)
                self.t3ch2.pulse_width_percent(0)
                self.t3ch1.pulse_width_percent(abs(duty))
                
            elif duty > 100: # positive duty means forward movement
                #print(duty)
                self.t3ch2.pulse_width_percent(0)
                self.t3ch1.pulse_width_percent(100)
                
            elif duty < 0: # negative duty means reverse movement
                #print(duty)
                self.t3ch1.pulse_width_percent(0)
                self.t3ch2.pulse_width_percent(abs(duty))
            else:
                self.disable()
            
        elif self.motortype == 2:
            if duty > 0 and duty <=100: # positive duty means forward movement
                #print(duty)
                self.t3ch4.pulse_width_percent(0)
                self.t3ch3.pulse_width_percent(abs(duty))
            
            elif duty > 100:
                #print(duty)
                self.t3ch4.pulse_width_percent(0)
                self.t3ch3.pulse_width_percent(100)
            elif duty < 0:
                #print(duty)
                self.t3ch3.pulse_width_percent(0)
                self.t3ch4.pulse_width_percent(abs(duty))
            else:
                self.disable()
        else:
            print('You cannot set the duty of a motor that does not exist.')
            pass


