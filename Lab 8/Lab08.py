'''
@file Lab08.py
@brief A file containing a script that tests my encoder driver and motor driver for Lab 08.
@details This file initializes 
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''

import pyb
import utime
import micropython
from MotorDriver8 import Motor
from EncoderDriver8 import Encoder
from ControllerTask8 import Controller

#Established memory reserved for displaying error messages
micropython.alloc_emergency_exception_buf(200)

encoder1 = Encoder(pyb.Pin.cpu.B6,pyb.Pin.cpu.B7,4,1000)
encoder2 = Encoder(pyb.Pin.cpu.C6,pyb.Pin.cpu.C7,8,1000)

pin_nSLEEP = pyb.Pin.cpu.A15
tim3 = pyb.Timer(3,freq=20000)
pin_nFAULT = pyb.Pin.cpu.B2
    
motor1_first_pin = pyb.Pin.cpu.B4
motor1_second_pin = pyb.Pin.cpu.B5
motor1 = Motor(pin_nSLEEP, motor1_first_pin, motor1_second_pin, tim3)

motor2_first_pin = pyb.Pin.cpu.B0
motor2_second_pin = pyb.Pin.cpu.B1
motor2 = Motor(pin_nSLEEP, motor2_first_pin, motor2_second_pin, tim3)
    

controller1 = Controller(encoder1,motor1)
controller2 = Controller(encoder2,motor2)
kill_state = 5
restore_state = 6

def myCallbackKillPower(line):
    """ 
    @brief Callback which runs when the user presses a the blue button on the Nucleo.
    @param line This parameter is needed to show when the callback has been activated.
    """
    controller1.TransitionTo(kill_state)
    controller1.TransitionTo(kill_state)
    print("Power Killed")
    
    # while response != "Y":
    #     response = input("Press 'Y' when you could like to resume running the motor.")
    #     time.delay(2)
    #     pass
    
def myCallbackRestorePower():
    """ 
    @brief This function restores power once the user wants to continue.
    """ 
    # #while pyb.Pin.cpu.B2.value() != 1:
    # while pyb.Pin.board.PC13.value() != 0:
    #     print("Fault was not fixed")
    #     pass
    controller1.TransitionTo(restore_state)
    controller2.TransitionTo(restore_state)
    
extInt = pyb.ExtInt(pyb.Pin.cpu.B2, pyb.ExtInt.IRQ_FALLING, pyb.Pin.OUT_PP, callback = myCallbackKillPower) # kills power if fault detected
extInt1 = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.OUT_PP, callback = myCallbackRestorePower) 
myuart = pyb.USB_VCP()
while True:
    try:
        utime.sleep_ms(1000)
        encoder1.update()
        encoder2.update()
        x1 = encoder1.get_delta()
        y1 = encoder2.get_delta()
        x = encoder1.get_position()
        y = encoder2.get_position()
        print("Motor 1 Position: " + str(x))
        print("Motor 1 Delta: " + str(x1))
        print("Motor 2 Position: " + str(y))
        print("Motor 2 Delta: " + str(y1))

    except KeyboardInterrupt:
        print("Program Closed")
        break
    
    
    
# Set up my controller class with a safety state that can restore power if the blue button is pressed. Use lab 02 for blue button stuff.
        # verify that the extint for fault condition works