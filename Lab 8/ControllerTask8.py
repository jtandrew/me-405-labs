'''
@file ControllerTask8.py
@brief A file containing the fsm for the lab. 
@details This file takes the input Kp value that was sent to the front-end, collects data for 5 seconds, and then sends that data back to the front-end for plotting. This file acts as the controller for the system.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''

import utime
import pyb
import array

class Controller:
    '''
    @brief      A class that sets up the data collection by acting as a finite state machine.
    @details    This class is a finite state machine that gets repeatly run by the main.py file. The main.py file runs as fast as possible, but this class controls the rate at which the data points are collected based on the desired data collection rate(50 ms).
    
    '''
    
    S0_init = 0
    S1_wait = 1
    
    S5_kill = 5
    S6_restore = 6
    
    def __init__(self,encoder,motor):
        '''
        @brief    This constructor initializes the data collection class and establishes the reference omega.  
        '''
        self.state = self.S0_init # makes sure that the encoder starts in the initial state when called for the first time
        self.encoder = encoder # makes an attribute that adds the Encoder Driver class
        self.motor = motor
        self.delta_t = 20 # ms
        self.myuart = pyb.USB_VCP()

    
    def run(self):
        
        '''
        @brief    This method is run repeatedly as the object transitions from waiting for a Kp, to collecting data and then sending the data. 
        '''
        self.current_time = utime.ticks_ms() #gets the current time
            
        if self.state == self.S0_init: # if this is the first time the machine is being run
            self.TransitionTo(self.S1_wait)
            
        elif self.state == self.S1_wait:
            print("waiting for fault")
            
        elif self.state == self.S5_kill: 
            self.motor.kill_power()
            
            
        elif self.state == self.S6_restore:
            self.motor.restore_power()
            self.TransitionTo(self.S1_wait)
            
        else:
            pass

    
    def TransitionTo(self,NewState):
        '''
        @brief      This method transitions the state of the object.
        '''
        self.state = NewState
        
       