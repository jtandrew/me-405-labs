'''
@file UserInterfaceLab8.py
@brief A file containing a script that calls the user to imput a Kp value. It also generates plots and analyzes the performance characteristic value J.
@details This script sends the Kp value to the Nucleo and generates the plots after receiving data from the Controller Task. This script acts as the front-end.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0

'''
import serial

ser = serial.Serial(port='COM5',baudrate=115273,timeout=16)  # leave a 10 second wait time

    

    




